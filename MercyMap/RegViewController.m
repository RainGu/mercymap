//
//  RegViewController.m
//  MercyMap
//
//  Created by sunshaoxun on 16/4/12.
//  Copyright © 2016年 Wispeed. All rights reserved.
//

#import "RegViewController.h"
#import "LoginService.h"
#import "VerificationViewController.h"
#import "ButtonAdd.h"
#import "AppDelegate.h"
@interface RegViewController ()<UITextFieldDelegate>
{
    LoginService *loginService;
    ButtonAdd *checklength;
}

@end

@implementation RegViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    checklength = [[ButtonAdd alloc]init];
    
    self.title =@"填写手机号";
    
    self.telPhoneTextfile.delegate =self;
    
    self.NextBtn.layer.masksToBounds =YES;
    self.NextBtn.layer.cornerRadius=10;
    
    self.NextBtn.backgroundColor=[UIColor colorWithRed:246/250.0 green:246/250.0 blue:246/250.0 alpha:1.0];
    
    loginService = [[LoginService alloc]init];
    [_telPhoneTextfile becomeFirstResponder];
    _telPhoneTextfile.keyboardType =UIKeyboardTypeNumberPad;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"navBackBtn@2x"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(navLeftBtnClick)];
    _NextBtn.backgroundColor =[UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
    
    self.NextBtn.layer.masksToBounds =YES;
    self.NextBtn.layer.cornerRadius=10;
    
}

-(void)navLeftBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 

- (IBAction)NextClick:(id)sender {
    
    if (![checklength checkInput:self.telPhoneTextfile.text] )
    {
        if ([self.telPhoneTextfile.text length]==11) {
            [loginService judegeMoibleExict:self.telPhoneTextfile.text SuccessBlock:^(NSString *success) {
                if ([success isEqualToString:@"S"]) {
                [CommoneTools alertOnView:self.view content:@"手机已注册"];
                }
                
                else {
                    
                     VerificationViewController *VerVC = [[VerificationViewController alloc]initWithNibName:@"VerificationViewController" bundle:nil];
                    [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:self.telPhoneTextfile.text zone: @"86" customIdentifier: nil result:^(NSError *error)
                     {
                         if (!error) {
                             VerVC.tel = self.telPhoneTextfile.text;
                             [self.navigationController pushViewController:VerVC animated:YES];
                             }
                         else{
                             [CommoneTools alertOnView:self.view content:@"发送失败"];
                             }
                     }];
                }
            }
            FailuerBlock:^(NSString *error) {
                
            }];
        }
        else
        {
            [CommoneTools alertOnView:self.view content:@"手机号错误"];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"gg:%@",textField.text);
    if ([checklength checkInput:string])
    {
        if([self.telPhoneTextfile.text length]==1)
          {
           _NextBtn.backgroundColor =[UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
           self.NextBtn.layer.masksToBounds =YES;
           self.NextBtn.layer.cornerRadius=10;
          }
          else
          {
              self.NextBtn.backgroundColor =[UIColor colorWithRed:34/255.0 green:139/255.0 blue:255/255.0 alpha:1.0];
              self.NextBtn.layer.masksToBounds =YES;
              self.NextBtn.layer.cornerRadius=10;
         }
    }
  else
    {
       self.NextBtn.backgroundColor =[UIColor colorWithRed:61/255.0 green:185/255.0 blue:253/255.0 alpha:1.0];
        self.NextBtn.layer.masksToBounds =YES;
        self.NextBtn.layer.cornerRadius=10;
    }
   
    return YES;
}

@end
