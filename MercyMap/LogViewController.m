//
//  LogViewController.m
//  MercyMap
//
//  Created by sunshaoxun on 16/4/12.
//  Copyright © 2016年 Wispeed. All rights reserved.
#import "LogViewController.h"
#import "AppDelegate.h"
#import "ButtonAdd.h"
#import "RegViewController.h"
#import "LoginService.h"
#import "PasswordViewController.h"
#import "Single.h"
#import "NSUserDefautSet.h"
#import <UMSocialCore/UMSocialCore.h>
@interface LogViewController (){
    LoginService *loginServic;
    Single *sing;
    NSUserDefautSet *defaultSet;
    AppDelegate *app;
}
@end
@implementation LogViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"登录";
//  self.userImageView.layer.cornerRadius =self.userImageView.frame.size.height/2;
    self.userImageView.layer.cornerRadius =10;
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.masksToBounds =YES;
    
    self.regBtn1.layer.masksToBounds =YES;
    self.regBtn1.layer.cornerRadius =YES;
    self.regBtn1.layer.cornerRadius =10;
    
    defaultSet = [[NSUserDefautSet alloc]init];
    sing = [Single Send];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"navBackBtn@2x"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(navLeftBtnClick)];
     _weixinBtn.hidden = YES;
     _forgetpassWord.hidden = YES;
}

-(void)navLeftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)logClick:(id)sender {
    ButtonAdd *length = [[ButtonAdd alloc]init];
    if ([length checkInput:self.telePhoneNumber.text]||[length checkInput:self.passwordText.text]) {
         [CommoneTools alertOnView:self.view content:@"请填写完整"];
    }
    else{
        [self loginUser:1];
     }
}

-(void)loginUser:(int)fag{
   loginServic = [[LoginService alloc]init];
   sing = [[Single alloc]init];
   sing = [Single Send];
    if (fag==1){
        sing.MobileNum = self.telePhoneNumber.text;
        sing.Password = self.passwordText.text;
    }
    [loginServic Login:sing.ID UserName:sing.MobileNum  andPassWord:sing.Password successBlock:^( NSMutableDictionary *dic) {
        if ([dic[@"Flag"]isEqualToString:@"S"]){
            sing = [[Single alloc]init];
            sing = [Single Send];
            sing.ID = [dic[@"ID"] intValue];
            sing.Token =dic[@"Token"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            //登录数据的基本存储
            [defaultSet loginDataStorage:sing.ID MobileNum:sing.MobileNum Password:sing.Password Token:sing.Token];
        }else{
            [CommoneTools alertOnView:self.view content:dic[@"Reason"]];
        }
    } FailuerBlock:^(NSString *error) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"网络不给力";
        hud.margin = 10.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        }];
}

- (IBAction)forgetPasswordBtn:(id)sender {
}

- (IBAction)regBtn:(id)sender {
    RegViewController *RegVC = [[RegViewController alloc]initWithNibName:@"RegViewController" bundle:nil];
    [self.navigationController pushViewController:RegVC animated:YES];
}

- (IBAction)QQLog:(id)sender {
    UMSocialPlatformType platformType = UMSocialPlatformType_Sina;
    [self sendUMeng:platformType ];
}

- (IBAction)weiboLog:(id)sender {
    UMSocialPlatformType platformType = UMSocialPlatformType_WechatSession;
    [self sendUMeng:platformType ];
}

- (IBAction)weixinlog:(id)sender{
    UMSocialPlatformType platformType = UMSocialPlatformType_Linkedin;
    [self sendUMeng:platformType ];
}

-(void)sendUMeng:(UMSocialPlatformType)PlatformType{
    [[UMSocialManager defaultManager] authWithPlatform:PlatformType currentViewController:self completion:^(id result, NSError *error) {
        NSString *message = nil;
        if (error) {
            message = @"登录失败";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"登录失败"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"确定",nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            [[UMSocialManager defaultManager] getUserInfoWithPlatform:PlatformType currentViewController:self completion:^(id result, NSError *error) {
                UMSocialUserInfoResponse *response = result;
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"登录失败"
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:NSLocalizedString(@"确定",nil)
                                                          otherButtonTitles:nil];
                    [alert show];
                }else{
                       loginServic =[[LoginService alloc]init];
                       sing =[Single Send];
                       sing.MobileNum = response.uid;
                       sing.Password  = response.uid;
                       sing.imageUrl  = response.iconurl;
                       sing.nickname  = response.name;
                       [[UMSocialManager defaultManager]cancelAuthWithPlatform:PlatformType completion:^(id result, NSError *error) {
                       }];
                      [loginServic Regist:sing.ID MobileNum:sing.MobileNum andPassWord:sing.Password Fag:2 successBlock:^(NSMutableDictionary *dic) {
                        if ([dic[@"Flag"]isEqualToString:@"S"]) {
//                        [self.navigationController popToRootViewControllerAnimated:YES];
                            [self secondLogin:2];
                        }
                        else{
//                         [CommoneTools alertOnView:self.view content:@"登录失败"];
                        [self loginUser:2];
                        }
                     } FailuerBlock:^(NSString *error) {
                    }];
                }
            }];
          }}];
}

-(void)secondLogin:(int)fag{
    loginServic =[[LoginService alloc]init];
    [loginServic Login:sing.ID UserName:sing.MobileNum  andPassWord:sing.Password successBlock:^( NSMutableDictionary *dic) {
        if ([dic[@"Flag"]isEqualToString:@"S"]){
            sing = [Single Send];
            sing.Token =dic[@"Token"];
            sing.ID = [dic[@"ID"] intValue];
            [defaultSet loginDataStorage:sing.ID MobileNum:sing.MobileNum Password:sing.Password Token:sing.Token];
           [self fixUser];
        }
        else{
            [CommoneTools alertOnView:self.view content:dic[@"Reason"]];
        }
    } FailuerBlock:^(NSString *error) {
        ButtonAdd *alterBtn =[[ButtonAdd alloc]init];
        [alterBtn AlterView:self];
    }];
}

-(void)fixUser{
   loginServic =[[LoginService alloc]init];
   [loginServic fixUserMessage:sing.ID Token:sing.Token Parameters:nil Code:@"ThirdLogin" successBlock:^(NSDictionary *model) {
       [self.navigationController popToRootViewControllerAnimated:YES];
       } Failuer:^(NSString *error) {
    }];
}
@end
