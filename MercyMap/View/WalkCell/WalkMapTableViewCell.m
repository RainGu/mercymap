//
//  WalkMapTableViewCell.m
//  MercyMap
//
//  Created by sunshaoxun on 16/9/16.
//  Copyright © 2016年 Wispeed. All rights reserved.
//

#import "WalkMapTableViewCell.h"
#import "UIButton+WebCache.h"
@implementation WalkMapTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self =[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.mapView];
    }
    return self;
}

-(MAMapView *)mapView{
  if (_mapView ==nil) {
      _mapView = [[MAMapView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.contentView.bounds), CGRectGetHeight(self.contentView.bounds))];
      _mapView.delegate = self;
     _mapView.mapType = MAMapTypeStandard;
     _mapView.showsScale = NO;
      CLLocationCoordinate2D center =CLLocationCoordinate2DMake(33.39,107.40);
      [_mapView setCenterCoordinate:center animated:YES];
    [_mapView setZoomLevel:4 animated:YES];
    }
    return _mapView;
}

-(void)sendDataMapView:(NSMutableArray *)dataArray{
    finallyArray = [NSMutableArray arrayWithCapacity:0];
    [finallyArray addObjectsFromArray:dataArray];
    for(i =0;i<dataArray.count;i++){
        if (![dataArray[i][@"ShopGPS"]isKindOfClass:[NSNull class]]){
            MapViewModel *annotation = [[MapViewModel alloc] init];
            NSString *str =dataArray[i][@"ShopGPS"];
            NSArray *array = [str componentsSeparatedByString:@","];
            annotation.title =dataArray[i][@"ShopName"];
            annotation.image = [UIImage imageNamed:@"walk"];
            annotation.subtitle = [NSString stringWithFormat:@"签到数：%d",[dataArray[i][@"SignInCount"] intValue]];
            float latitude = [array[0] floatValue];
            float longitude =[array[1] floatValue];
            annotation.coordinate =CLLocationCoordinate2DMake(latitude ,longitude);
            [_mapView addAnnotation:annotation];
        }
    }
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation{
    if ([annotation isKindOfClass:[MapViewModel class]]){
        static NSString *animatedAnnotationIdentifier = @"AnimatedAnnotationIdentifier";
        MAPinAnnotationView  *annotationView = (MAPinAnnotationView  *)[mapView dequeueReusableAnnotationViewWithIdentifier:animatedAnnotationIdentifier];
        if (annotationView == nil){
            annotationView = [[MAPinAnnotationView  alloc] initWithAnnotation:annotation
                                                              reuseIdentifier:animatedAnnotationIdentifier];
            annotationView.animatesDrop     = NO;
            annotationView.canShowCallout   = YES;
            annotationView.highlighted      = YES;
        }
        annotationView.annotation = annotation;
        annotationView.image      = ((MapViewModel *)annotation).image;//设置大头针视图的图片
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view{
    _Mapcoordinate =view.annotation.coordinate;
}
@end
