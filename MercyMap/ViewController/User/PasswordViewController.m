//
//  PasswordViewController.m
//  MercyMap
//
//  Created by sunshaoxun on 16/4/15.
//  Copyright © 2016年 Wispeed. All rights reserved.
//

#import "PasswordViewController.h"
#import "ButtonAdd.h"
#import "LogViewController.h"
#import "LoginService.h"
#import "Single.h"
@interface PasswordViewController ()<UITextFieldDelegate>
{
    ButtonAdd *lengthCheck;
    LoginService *serVice;
    Single *Sing;
}
@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"输入密码";
    
    self.OverBtn1.layer.masksToBounds =YES;
    self.OverBtn1.layer.cornerRadius=10;
    lengthCheck = [[ButtonAdd alloc]init];
    self.passWordFiled.delegate =self;
    self.againPasswordField.delegate =self;
    serVice = [[LoginService alloc]init];
    Sing = [Single Send];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"navBackBtn@2x"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(navLeftBtnClick)];
}

-(void)navLeftBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField.text length]<5||[self.againPasswordField.text length]<5)
    {
        _OverBtn1.backgroundColor =[UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
        self.OverBtn1.layer.masksToBounds =YES;
        self.OverBtn1.layer.cornerRadius=10;
    }
    else
    {
        self.OverBtn1.backgroundColor =[UIColor colorWithRed:34/255.0 green:139/255.0 blue:255/255.0 alpha:1.0];
        self.OverBtn1.layer.masksToBounds =YES;
        self.OverBtn1.layer.cornerRadius=10;
    }
    return YES;
}

- (IBAction)OverBtn:(id)sender {
    
    if ([lengthCheck checkInput:self.passWordFiled.text]||[lengthCheck checkInput:self.againPasswordField.text] )
    {
         [CommoneTools alertOnView:self.view content:@"请填写完整"];
    }
    
    else{
    if(![self.passWordFiled.text isEqualToString:self.againPasswordField.text])
        {
          [CommoneTools alertOnView:self.view content:@"两次密码不同"];
        }
    else
        {
            if ([self.passWordFiled.text length]>=6 && [self.passWordFiled.text length]<=16) {
                [serVice Regist:Sing.ID MobileNum:self.telNum andPassWord:self.passWordFiled.text Fag:1 successBlock:^(NSMutableDictionary *dic) {
                    if ([dic[@"OMsg"][@"Flag"]isEqualToString:@"S"]) {
                       LogViewController *LoginVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-4];
                        LoginVC.telePhoneNumber.text = self.telNum;
                        LoginVC.passwordText.text =self.passWordFiled.text;
                        [self.navigationController popToViewController:LoginVC animated:YES];
                    }
                    else{
                         [CommoneTools alertOnView:self.view content:@"注册失败"];
                        }
                } FailuerBlock:^(NSString *error) {
                    
                }];
            }
            else
            {
             [CommoneTools alertOnView:self.view content:@"密码长度错误"];
            }
        }
    }
}
@end
